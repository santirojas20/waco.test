#!/bin/ash
chmod 777 storage
composer install
# Habia un problema con el passport, asi que instale desde composer 
composer require laravel/passport
php artisan key:generate
php artisan migrate:refresh --seed
php artisan passport:install --force
php artisan test
